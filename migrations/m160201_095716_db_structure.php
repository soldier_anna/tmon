<?php

use yii\db\Schema;
use yii\db\Migration;

class m160201_095716_db_structure extends Migration
{
    public function up()
    {
        // Accounts
        $this->createTable('users',[
            'user_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'user_login' => "VARCHAR(255) NOT NULL COMMENT 'Имя пользователя - емеил'",
            'user_pass' => "VARCHAR(255) NOT NULL COMMENT 'Пароль пользователя SHA(UPPER(user_login:user_pass))'",
            'auth_key' => "VARCHAR(32) NOT NULL COMMENT 'Нужно для работы авторизации в yii'",
            'password_reset_token' => "VARCHAR(255) NOT NULL COMMENT 'Нужно для работы авторизации в yii'",
            'user_nick' => "VARCHAR(255) NOT NULL COMMENT 'Никнейм для отображения в чате'",
            'user_role' => "ENUM('customer','agent','manager','administrator') NOT NULL DEFAULT 'customer' COMMENT 'Роль учетной записи'",
            'user_state' => "ENUM('inactive','active','disable') NOT NULL DEFAULT 'inactive' COMMENT 'Состояние'",
            'user_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('user_login_unique', 'users', 'user_login', true);
        $this->createIndex('user_nick_unique', 'users', 'user_nick', true);

        $fieldsUsers = ['user_id', 'user_login', 'user_pass', 'user_nick', 'user_role' , 'user_state', 'user_create'];
        $this->batchInsert('users', $fieldsUsers, [ 
            [1, 'admin',  new \yii\db\Expression("SHA1(CONCAT(UPPER('admin'), ':', '321654'))"), 'Admin', 'administrator', 'active', '2016-02-01 00:00:00'],
            [2, 'manager', new \yii\db\Expression("SHA1(CONCAT(UPPER('manager'), ':', '321654'))"), 'Manager', 'manager', 'active', '2016-02-01 01:00:00'],
            [3, 'agent1', new \yii\db\Expression("SHA1(CONCAT(UPPER('agent1'), ':', '321654'))"), 'Agent 1', 'agent', 'active', '2016-02-01 02:00:00'],
            [4, 'agent2', new \yii\db\Expression("SHA1(CONCAT(UPPER('agent2'), ':', '321654'))"), 'Agent 2', 'agent', 'active', '2016-02-01 03:00:00'],
            [5, 'agent3', new \yii\db\Expression("SHA1(CONCAT(UPPER('agent3'), ':', '321654'))"), 'Agent 3', 'agent', 'active', '2016-02-01 04:00:00'],
            [6, 'customer', new \yii\db\Expression("SHA1(CONCAT(UPPER('customer'), ':', '321654'))"), 'Customer', 'customer', 'active', '2016-02-01 05:00:00']           
        ]);
        /* Связи пользователя с чатом */
        $this->createTable('chatsusers', [
            'chatuser_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'user_id' => "INT UNSIGNED NOT NULL COMMENT 'Привязка к пользователю'",
            'chat_id' => "INT UNSIGNED NOT NULL COMMENT 'Привязка к чату'",
            'chatuser_role' => "ENUM('member','owner') NOT NULL DEFAULT 'member' COMMENT 'Состояние (member - обычный участник, owner - владелец)'",
            'chatuser_state' => "ENUM('invite','request','inactive','active','typing','leave') NOT NULL DEFAULT 'invite' COMMENT 'Состояние (active - находится в чате, запрашивает данны из чата, typing - набирает текст, request - запрос на разрешение войти, invite - приглашение на вход, leave - покинуть чат)'",
            'chatuser_update' => "TIMESTAMP NOT NULL DEFAULT  '2000-01-01 00:00:00' COMMENT 'Дата обновления статуса'",
            'chatuser_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'"
        ], 'ENGINE=InnoDB CHARSET=utf8');
        /* Чат румы */
        $this->createTable('chats', [
            'chat_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к создателю чата'",
            'product_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к товару'",
            'chat_type' => "ENUM('public','private','product','agency') NOT NULL DEFAULT 'public' COMMENT 'Разновидность чата'",
            'chat_state' => "ENUM('open','assigned','close') NOT NULL DEFAULT 'open' COMMENT 'Состояние'", 
            'chat_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'"
        ], 'ENGINE=InnoDB CHARSET=utf8');
        /* Связи пользователя с сообщением (заполняется в случае ограничения видящих, например при mention - упоминании, чтобы видели только те кого упомянули, иначе видят все) */
        $this->createTable('messagesusers', [
            'messageuser_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'message_id' => "INT UNSIGNED NOT NULL COMMENT 'Сообщение'",
            'user_id' => "INT UNSIGNED NOT NULL COMMENT 'Пользователь имеющий к нему доступ'",
            'messageuser_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'"
        ], 'ENGINE=InnoDB CHARSET=utf8');
        /* Сообщения */
        $this->createTable('messages', [
            'message_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'chat_id' => "INT UNSIGNED NOT NULL COMMENT 'Чат в котором это сообщение сотавлено'",
            'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Владелец сообщения'",
            'message_type' => "ENUM('text','emotion','file','mention') NOT NULL DEFAULT 'text' COMMENT 'Разновидность сообщения/Тип данных (mention - упоминание)'",
            'message_data' => 'TEXT NOT NULL COMMENT \'Содержимое сообщения в json формате {"text":"asdasd [[f:0]] wew [[e:2]]", "html":"", "files":[{"id":"","progress":"","name":"","size":"","url":""}], "emotions":[{"emotion_id":""}]}\'',
            'message_state' => "ENUM('received','updated') NOT NULL DEFAULT 'received' COMMENT 'Состояние сообщения (received - получено сервером, updated - обновлено)'",
            'message_update' => "TIMESTAMP NOT NULL DEFAULT  '2000-01-01 00:00:00'  COMMENT 'Дата последнего обновления'",
            'message_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",
        ], 'ENGINE=InnoDB CHARSET=utf8');

        /* База доставленности сообщений */
        $this->createTable('delivmessagesusers', [
            'message_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Сообщение'",
            'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Владелец сообщения'",
            'delivmessageuser_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->addPrimaryKey('mes_user', 'delivmessagesusers', ['message_id', 'user_id']);

        /* сами прикрпеленные файлы можно упоминать в text или html с использование тех метов "[[:f0:]]" с указанием индекса
Эмоции так и будут передаваться "[[:e4:]]" первая буква тип объекта или спец вставки, а дальше индекс или ИД объекта */

/* Для фиксирования доставки нужно завести отдельную таблицу с записями доставки этого сообщения каждому пользователю */

/* Хранилище файлов, оно должно быть независимо от чата, и обслуживаться отдельно с доступом по связке с юзером */
/*  Тоесть сообщение уходит с тем что файл прикреплен и заливается, права на файл определяются при его заливке в самой системе хранения файлов,
    паралельно идет заливка файла через систему хранения файлов, и обновление сообщения с указанием прогресса заливки, 
    по окончании заливки обновляется сообщение и в него заносится конечный урл до файла */

        $this->createTable('files', [
            'file_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Связка с владельцем'",
            'chat_id' => "INT UNSIGNED NOT NULL COMMENT 'Чат в котором этот файл публикуется'",
            'file_title' => "VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Название файла'",
            'file_url' => "TEXT NOT NULL COMMENT 'Содержимое урл до файла'",
            'file_type' => "ENUM('image','word','excel','archiv','sound','video','other') NOT NULL DEFAULT 'other' COMMENT 'Тип файла'",
            'file_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",

        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createTable('filesaccesses', [
            'fileaccess_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT  PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'file_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Связка с файлом'",
            'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Связка с пользователем'",
            'fileaccess_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'"
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createTable('emotions', [
            'emotion_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'emotion_key' => "VARCHAR(255) NOT NULL COMMENT 'Уникальный строковый идентификатор'",
            'emotion_title' => "VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Название эмоции'",
            'emotion_url' => "TEXT NOT NULL COMMENT 'Содержимое урл до картинки'",
            'emotion_state' => "ENUM('inactive','active') NOT NULL DEFAULT 'inactive' COMMENT 'Состояние эмоции'",
            'emotion_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('emotion_key_unique', 'emotions', 'emotion_key', true);

        $this->createTable('groups', [
            'group_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'group_title' => "VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Название группы'",
            'group_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",

        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createTable('groupsusers', [
            'groupuser_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'user_id' => "INT UNSIGNED NOT NULL COMMENT 'Привязка к пользователю'",
            'group_id' => "INT UNSIGNED NOT NULL COMMENT 'Привязка к чату'",
            'groupuser_create' => " TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",

        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('group_user_unique', 'groupsusers', ['user_id', 'group_id'], true);

        $this->createTable('rules', [
            'rule_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'rule_title' => "VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Название правила'",
            'rule_info' => 'TEXT NOT NULL COMMENT \'Содержимое список правил в json формате {"type":"AND|OR","keywords":["word1", "word2"]}\'',
            'rule_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createTable('rulesgroupsusers',[
            'rulegroupuser_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'rule_id' => "INT UNSIGNED NOT NULL COMMENT 'Привязка к правилу'",
            'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к пользователю'",
            'group_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к чату'",
            'rulegroupuser_create' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'",
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('rule_group_user_unique', 'rulesgroupsusers', ['rule_id', 'user_id', 'group_id']);

        //ALTER TABLE `users` ADD `auth_key` VARCHAR(32) NOT NULL AFTER `user_pass`, ADD `password_reset_token` VARCHAR(255) NOT NULL AFTER `auth_key`;
    }



    public function down()
    {
        $this->dropTable('users');
        $this->dropTable('chatsusers');
        $this->dropTable('chats');
        $this->dropTable('messagesusers');
        $this->dropTable('messages');
        $this->dropTable('delivmessagesusers');
        $this->dropTable('files');
        $this->dropTable('filesaccesses');
        $this->dropTable('emotions');
        $this->dropTable('groups');
        $this->dropTable('groupsusers');
        $this->dropTable('rules');
        $this->dropTable('rulesgroupsusers');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
