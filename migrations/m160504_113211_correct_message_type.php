<?php

use yii\db\Migration;

class m160504_113211_correct_message_type extends Migration
{
    public function up()
    {
        $this->delete('automessages');
	$this->alterColumn('automessages', 'message_type', "ENUM('text', 'emotion', 'file', 'mention', 'product', 'order', 'advert', 'url', 'auto_vote', 'auto_greeting', 'auto_allocation', 'auto_offhours', 'btn_order', 'btn_lastseen', 'btn_basket') NOT NULL DEFAULT 'text' COMMENT '������������� ���������/��� ������ (mention - ����������)'");
        $this->batchInsert('automessages', ['automessage_id', 'user_id', 'group_id', 'rule_id', 'automessage_type', 'message_type', 'message_data', 'automessage_create'], [ 
            [1, 0, 0, 0, 'Allocation', 'auto_allocation', '{"text":"This is Allocation !"}', '2016-02-01 05:00:00'],
            [2, 0, 0, 0, 'Greeting', 'auto_greeting', '{"text":"This is Greeting !"}', '2016-02-01 05:00:00'],
            [3, 5, 0, 0, 'Greeting', 'auto_greeting', '{"text":"This is Greeting for agent 5 !"}', '2016-02-01 05:00:00'],
            [4, 5, 0, 0, 'OffHours', 'auto_offhours', '{"text":"Off hours time !"}', '2016-02-01 05:00:00'],
        ]);

        $this->delete('messages');
	$this->alterColumn('messages', 'message_type', "ENUM('text', 'emotion', 'file', 'mention', 'product', 'order', 'advert', 'url', 'auto_vote', 'auto_greeting', 'auto_allocation', 'auto_offhours', 'btn_order', 'btn_lastseen', 'btn_basket') NOT NULL DEFAULT 'text' COMMENT '������������� ���������/��� ������ (mention - ����������)'");
    }

    public function down()
    {
        echo "m160504_113211_correct_message_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
