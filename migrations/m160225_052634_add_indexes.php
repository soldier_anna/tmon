<?php

use yii\db\Schema;
use yii\db\Migration;

class m160225_052634_add_indexes extends Migration
{
    public function up()
    {
        $this->createIndex('chat_user_unique', 'chatsusers', ['user_id', 'chat_id'], true);
    }
 
    public function down()
    {
        echo "m160225_052634_add_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
