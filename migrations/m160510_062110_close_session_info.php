<?php

use yii\db\Migration;

class m160510_062110_close_session_info extends Migration
{
    public function up()
    {
	/* ���������� � �������� ���-������ */
        $this->createTable('closeinfo',[
		'closeinfo_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT '���������� �������������'",
		'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '������ � ������������� ��������� ���'",
		'session_id' => "INT UNSIGNED NOT NULL COMMENT '���-������ � ������� ��������� ������'",
		'closeinfo_type' => "ENUM('solved', 'unsolved') NOT NULL DEFAULT 'solved' COMMENT '������������� ����������'",
		'closeinfo_data' => "TEXT NOT NULL COMMENT '���������� ��������� � json {}'",
		'closeinfo_create' => "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'",
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('closeinfo_unique', 'closeinfo', ['session_id'], true);
    }

    public function down()
    {
        echo "m160510_062110_close_session_info cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
