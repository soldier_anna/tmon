<?php

use yii\db\Schema;
use yii\db\Migration;

class m160303_100448_add_chatsrelations extends Migration
{
    public function up()
    {

        $this->createTable('chatsrelations', [
            'chatrelation_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный идентификатор'",
            'chat_id' => "INT UNSIGNED NOT NULL COMMENT 'Привязка к чату'",
            'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к пользователю'",
            'group_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к группе'",
            'chatrelation_role' => "ENUM('member','owner') NOT NULL DEFAULT 'member' COMMENT 'Состояние (member - обычный участник, owner - владелец)'",
            'chatrelation_state' => "ENUM('invite','request','inactive','active','typing','leave') NOT NULL DEFAULT 'invite' COMMENT 'Состояние (active - находится в чате, запрашивает данны из чата, typing - набирает текст, request - запрос на разрешение войти, invite - приглашение на вход, leave - покинуть чат)'",
            'chatrelation_update' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата обновления статуса'",
            'chatrelation_create' => "TIMESTAMP NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT 'Дата создания записи'"
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('chat_relation_unique', 'chatsrelations', ['chat_id', 'user_id', 'group_id'] );

        /*DROP TABLE IF EXISTS chatsrelations;
        CREATE TABLE IF NOT EXISTS chatsrelations (
          chatrelation_id INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',

          chat_id INT UNSIGNED NOT NULL COMMENT 'Привязка к чату',
          user_id INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к пользователю',
          group_id INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Привязка к группе',

          chatrelation_role ENUM('member','owner') NOT NULL DEFAULT 'member' COMMENT 'Состояние (member - обычный участник, owner - владелец)',
          chatrelation_state ENUM('invite','request','inactive','active','typing','leave') NOT NULL DEFAULT 'invite' COMMENT 'Состояние (active - находится в чате, запрашивает данны из чата, typing - набирает текст, request - запрос на разрешение войти, invite - приглашение на вход, leave - покинуть чат)',
          chatrelation_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата обновления статуса',

          chatrelation_create TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи',
          PRIMARY KEY (`chatrelation_id`),
          UNIQUE KEY chat_relation_unique (`chat_id`, user_id, `group_id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8; */
    }

    public function down()
    {
        echo "m160303_100448_add_chatsrelations cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
