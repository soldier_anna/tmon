<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_130239_create_products_orders_tbls extends Migration
{
    public function up()
    {

        /*$fieldsUsers = ['user_id', 'user_login', 'user_pass', 'user_nick', 'user_role' , 'user_state', 'user_create'];
        $this->batchInsert('users', $fieldsUsers, [ 
            [10, 'customer2',  new \yii\db\Expression("SHA1(CONCAT(UPPER('customer2'), ':', '321654'))"), 'customer 2', 'customer', 'active', '2016-02-01 00:00:00'],
            [11, 'customer3', new \yii\db\Expression("SHA1(CONCAT(UPPER('customer3'), ':', '321654'))"), 'customer 3', 'customer', 'active', '2016-02-01 01:00:00'],
            [12, 'customer4', new \yii\db\Expression("SHA1(CONCAT(UPPER('customer4'), ':', '321654'))"), 'customer 4', 'customer', 'active', '2016-02-01 02:00:00']
                     
        ]);*/

        /*$this->createTable('products', [
            'id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
            'name' => "VARCHAR(255) NOT NULL",
            'price' => "DECIMAL(11,2) unsigned NOT NULL DEFAULT 0 COMMENT 'Price'",
            'img' => "VARCHAR(255) NOT NULL DEFAULT ''",
            'url' => "VARCHAR(255) NOT NULL DEFAULT ''"            
        ]);

        $fieldsProducts = ['id', 'name',  'price', 'img', 'url'];
        $this->batchInsert('products', $fieldsProducts, [
            [1, 'Product 1', 560.00, 'img1.jpg', 'http://test1' ],
            [2, 'Product2', 234.00, 'img2.jpg', 'http://test2' ],
            [3, 'Product 3', 678.00, 'img3.jpg', 'http://tes21' ],
            [4, 'Product 4', 90.00, 'img4.jpg', 'http://test16' ],
            [5, 'Product 5', 1233.00, 'img5.jpg', 'http://test91' ]

        ]);


        $this->createTable('orders', [
            'order_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
            'product_id' => "INT UNSIGNED NOT NULL",
            'state' => "ENUM('pending','complete','cancelled','shipped', 'returned') NOT NULL DEFAULT 'pending'",
            'qty' => "INT NOT NULL DEFAULT 0",
            'date_created' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
            'total_sum' => "DECIMAL(11,2) unsigned NOT NULL DEFAULT 0 COMMENT 'Total sum of order'",
            'user_id' => "INT UNSIGNED NOT NULL COMMENT 'Привязка к пользователю'"
        ]);
        $fieldsOrders = ['product_id', 'state', 'qty', 'date_created', 'total_sum','user_id'];
        $this->batchInsert('orders', $fieldsOrders, [
            [ 1, 'pending', 3, '2016-01-17 12:30:45', 1680, 6 ],
            [ 2, 'pending', 1, '2016-01-17 12:34:45', 234, 6 ],
            [ 5, 'complete', 1, '2016-01-18 12:34:45', 1233, 10 ],
            [ 3, 'pending', 1, '2016-01-19 12:34:45', 678, 10 ]
        ]);
        */
        //////////

        $this->createTable('products', [
            'id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
            'main_deal_srl' => "INT(20) NOT NULL",
            'name' => "VARCHAR(100) NOT NULL",
            'description' => "TEXT NOT NULL",
            'delivery' => "ENUM('fee','free') NOT NULL DEFAULT 'free'",
            'price' => "DECIMAL(11,2) unsigned NOT NULL DEFAULT 0 COMMENT 'Price'",
            'category_srl' => "VARCHAR(200) NOT NULL",
            'img' => "VARCHAR(255) NOT NULL DEFAULT ''",
            //'url' => "VARCHAR(255) NOT NULL DEFAULT ''"            
        ], 'ENGINE=InnoDB CHARSET=utf8');


        $fieldsProducts = ['id', 'main_deal_srl', 'name', 'description', 'delivery', 'price', 'category_srl', 'img'];
        $this->batchInsert('products', $fieldsProducts, [
            [1, 2001, 'Product 1', '100% cotton', 'free', 560.00, 'clothes', 'by512412.jpg' ],
            [2, 2002, 'Product 2', '100% cotton', 'free', 234.00, 'clothes', 'by513450.jpg' ],
            [3, 2003, 'Product 3', '100% cotton', 'fee', 678.00, 'clothes', 'by522402.jpg' ],
            [4, 2004, 'Product 4', '100% cotton', 'fee', 90.00, 'clothes', 'by522410.jpg' ],
            [5, 2005, 'Product 5', '100% cotton',  'free', 1233.00, 'clothes',  'by511500.jpg' ],
            [6, 111, 'Faux Suede Western Heel Ankle Bootsa', 'Heel height: 5cm',  'free', 22.00, 'boots',  'bk611458.jpg' ]

        ]);

        $this->createTable('orders', [
            'id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
            'main_buy_srl' => "INT(20) NOT NULL", 
            'main_deal_srl' => "INT(20) NOT NULL",           
            'date' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
            'count' => "INT NOT NULL DEFAULT 0",            
            'state' => "ENUM('cancelled','success', 'returned') NOT NULL DEFAULT 'success'",
            'original_price' => "DECIMAL(11,2) unsigned NOT NULL DEFAULT 0 ",
            'discount_price' => "DECIMAL(11,2) unsigned NOT NULL DEFAULT 0 ",
            'coupon' => "VARCHAR(100) NOT NULL DEFAULT ''",
            'delivery_addr' => "VARCHAR(255) NOT NULL DEFAULT ''",
            'account_srl' => "INT UNSIGNED NOT NULL"
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $fieldsOrders = ['id', 'main_buy_srl', 'main_deal_srl', 'date', 'count','state', 
                        'original_price', 'discount_price', 'coupon', 'delivery_addr','account_srl'];
        $this->batchInsert('orders', $fieldsOrders, [
            [ 1,3001, 2001, '2016-01-17 12:30:45', 3,  'success', 560.00, 500.00, 277, '#2, 304, kangnam-gu, seoul, korea', 6 ],
            [ 2,3002, 2002, '2016-01-17 13:30:45', 3,  'success', 234.00, 200.00, 277, '#2, 304, kangnam-gu, seoul, korea', 6 ],
            [ 3,3003, 2001, '2016-01-18 15:30:45', 3,  'returned', 560.00, 500.00, 277, '#7, 278, kangnam-gu, seoul, korea', 10 ],
            [ 4,3004, 2005, '2016-01-20 18:56:45', 3,  'cancelled', 1233.00, 1000.00, 277, '#2, 304, kangnam-gu, seoul, korea', 6 ]
        ]);

        $this->createTable('cart_items', [
            'id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
            'cart_srl' => "INT(20) NOT NULL", 
            'main_deal_srl' => "INT(20) NOT NULL", 
            'remain_time' => "INT NOT NULL DEFAULT 0",
            'account_srl' => "INT UNSIGNED NOT NULL"
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $fieldsCart = ['id', 'cart_srl', 'main_deal_srl', 'remain_time', 'account_srl'];
        $this->batchInsert('cart_items', $fieldsCart, [
            [1, 4009, 2001, 5, 6],
            [2, 4010, 2002, 4, 6],
            [3, 4011, 2001, 3, 10],
            [4, 4012, 2005, 7, 6]
        ]);

        $this->createTable('mobiles', [
            'id' => 'int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'id_user' => "int unsigned NOT NULL COMMENT 'Владелец записи'",
            'device_type' => "enum('android','ios') NOT NULL DEFAULT 'android' COMMENT 'Тип девайс токена'",
            'device_token' => "TEXT NOT NULL COMMENT 'Токен девайса для пушей'",
            'create_at' => "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи'"
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createTable('pushes', [
            'push_id' => 'int NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'push_type' => "ENUM('android','ios') NOT NULL COMMENT 'Тип устройства'",
            'push_key_origin' => "TEXT COMMENT 'Ключи список для отправки [\"key1\", \"key2\", ...]'",
            'push_key_failed' => "TEXT COMMENT 'Ключи на которые отправки небыло [\"key1\", \"key2\", ...]'",
            'push_data' => "TEXT COMMENT 'данные в формате JSON {\"title\":\"заголовок\", \"message\":\"Текст сообщения\", \"image\":\"urlToImage\", ...}'",
            'push_state' => "ENUM('Request', 'SendAll', 'SendPart', 'Error') NOT NULL DEFAULT 'Request' COMMENT 'Request - новая заявка, SendAll - отправлены все, SendPart - отправка произведена частично, Error - возникла ошибка при отправке'",
            'push_info_send' => "TEXT COMMENT 'Информация об отправке'",
            'create_at' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ], 'ENGINE=InnoDB CHARSET=utf8');

    }

    public function down()
    {
        echo "m160202_130239_create_products_orders_tbls cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
