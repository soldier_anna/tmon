<?php

use yii\db\Schema;
use yii\db\Migration;

class m160301_042455_alter_products_order_rels extends Migration
{
    public function up()
    {
        $this->createTable('order_products', [
            'id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
            'order_id' => "INT UNSIGNED NOT NULL",
            'product_id' => "INT UNSIGNED NOT NULL"                     
        ]);
    }

    public function down()
    {
        echo "m160301_042455_alter_products_order_rels cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
