<?php

use yii\db\Migration;

class m160503_111015_message_fix_tipe extends Migration
{
    public function up()
    {
        $this->delete('messages');
	$this->alterColumn('messages', 'message_type', "ENUM('text', 'emotion', 'file', 'mention', 'product', 'order', 'advert', 'url', 'vote', 'greeting', 'offhours', 'allocation') NOT NULL DEFAULT 'text' COMMENT '������������� ���������/��� ������ (mention - ����������)'");
    }

    public function down()
    {
        echo "m160503_111015_message_fix_tipe cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
