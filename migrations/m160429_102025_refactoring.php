<?php

use yii\db\Migration;

class m160429_102025_refactoring extends Migration
{
    public function up()
    {
	/* ������� ������� �� ���������� */
	//cart_items
	//mobiles
	//settings
	//order_products
	//orders
	//products
	//pushes

	/* �������� ������� */
        $this->dropTable('chats');
        $this->dropTable('chatsrelations');
        $this->dropTable('chatsusers');
        $this->dropTable('messagesusers');
        $this->dropTable('rulesgroupsusers');

	/* ��� ������ �� ������ ������� ����� */
        $this->createTable('sessions',[
		'session_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT '���������� �������������'",
		'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ��������� ����'",
		'session_state' => "ENUM('unassigned','assigned','allocated','close') NOT NULL DEFAULT 'unassigned' COMMENT '��������� (unassigned - ����� ��� �� ��������, assigned - ��������� � ������, allocated - ��������� ��������, close - ������ �������)'",
		'session_update' => "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� ���������� ���������� session_state'",
		'session_create' => "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'",
        ], 'ENGINE=InnoDB CHARSET=utf8');

	/* ���� ������������� � ���������� � ��� */
        $this->delete('users');
        $this->alterColumn('users', 'user_state', "ENUM('Training','Lunch','O/B','Online') NOT NULL DEFAULT 'Online' COMMENT '��������� ��� �������� ����������, Logout - ���� �������'");
        $this->addColumn('users', 'user_capacity', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '���������� ������������ �������������� ���-������ �������'");
        $this->alterColumn('users', 'user_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");
        $this->batchInsert('users', ['user_id', 'user_login', 'user_pass', 'user_nick', 'user_role' , 'user_state', 'user_capacity', 'auth_key', 'password_reset_token', 'user_create'], [ 
            [1, 'admin',  new \yii\db\Expression("SHA1(CONCAT(UPPER('admin'), ':', '321654'))"), 'Admin', 'administrator', 'Online', 5, '', '', '2016-02-01 00:00:00'],
            [2, 'manager', new \yii\db\Expression("SHA1(CONCAT(UPPER('manager'), ':', '321654'))"), 'Manager', 'manager', 'Online', 4, '', '', '2016-02-01 01:00:00'],
            [3, 'agent1', new \yii\db\Expression("SHA1(CONCAT(UPPER('agent1'), ':', '321654'))"), 'Agent 1', 'agent', 'Online', 7, '', '', '2016-02-01 02:00:00'],
            [4, 'agent2', new \yii\db\Expression("SHA1(CONCAT(UPPER('agent2'), ':', '321654'))"), 'Agent 2', 'agent', 'Training', 4, '', '', '2016-02-01 03:00:00'],
            [5, 'agent3', new \yii\db\Expression("SHA1(CONCAT(UPPER('agent3'), ':', '321654'))"), 'Agent 3', 'agent', 'Lunch', 5, '', '', '2016-02-01 04:00:00'],
            [6, 'customer', new \yii\db\Expression("SHA1(CONCAT(UPPER('customer'), ':', '321654'))"), 'Customer', 'customer', 'Online', 0, '', '', '2016-02-01 05:00:00']           
        ]);

	/* ���������� ����������� */
        $this->createTable('votes',[
		'session_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ���-������'",
		'vote_result' => "INT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '������ ������'",
		'vote_create' => "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'",
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('session_id_unique', 'votes', 'session_id', true);

	/* ������������� �������� */
        $this->createTable('routings',[
		'routing_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT '���������� �������������'",
		'session_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ���-������'",
		'rule_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ������� ��������'",
		'message_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ��������� �� �������� ���������� �������'",
		'routing_create' => "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'",
        ], 'ENGINE=InnoDB CHARSET=utf8');

	/* ������� ��� �������� */
	$this->addColumn('rules', 'user_id', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ������������'");
	$this->addColumn('rules', 'group_id', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ������'");
        $this->alterColumn('rules', 'rule_info', "TEXT NOT NULL COMMENT '���������� ������ ������ � json ������� {\'type\':\'AND|OR\',\'conditions\':[{\'key\':\'message\', \'value\':\'example\', \'strict\':\'complete|overlap\'}]}, complete - ������ ����������, overlap - ��������� �������'");
	$this->addColumn('rules', 'rule_state', "ENUM('enabled','disabled') NOT NULL DEFAULT 'disabled' COMMENT '���������'");
	$this->addColumn('rules', 'rule_priority', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '��������� ������'");
        $this->alterColumn('rules', 'rule_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");

	/* ������ */
	$this->addColumn('groups', 'group_thumb', "TEXT NOT NULL COMMENT 'Url �� ������ ������'");
	$this->addColumn('groups', 'group_backup', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������������� ������, ���� ������� �����������'");
	$this->addColumn('groups', 'group_capacity', "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '����������� ������'");
	$this->alterColumn('groups', 'group_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");

	/* ������ ������������� � �������� */
	$this->alterColumn('groupsusers', 'groupuser_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");

	/* ������ ������������� � ���-�������� */
        $this->createTable('sessionsrelations',[
		'sessionrelation_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT '���������� �������������'",
		'session_id' => "INT UNSIGNED NOT NULL COMMENT '�������� � ���-������'",
		'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ������������'",
		'group_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������� � ������'",
		'sessionrelation_role' => "ENUM('member', 'manager', 'owner') NOT NULL DEFAULT 'member' COMMENT '��������� (member - ������� ��������, manager - �����������)'",
		'sessionrelation_state' => "ENUM('active','leave') NOT NULL DEFAULT 'active' COMMENT '��������� (active - ��������� � ����, ����������� ����� �� ����, leave - ������� ���)'",
		'sessionrelation_update' => "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� ���������� �������'",
		'sessionrelation_create' => "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'",
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('session_user_group_unique', 'sessionsrelations', ['session_id', 'user_id', 'group_id'], true);

	/* ���� � ����������� */
	$this->dropColumn('messages', 'chat_id');
	$this->dropColumn('messages', 'group_id');
	$this->addColumn('messages', 'session_id', "INT UNSIGNED NOT NULL COMMENT '���-������ � ������� ��� ��������� ���������'");
	$this->alterColumn('messages', 'message_type', "ENUM('text', 'emotion', 'file', 'mention', 'product', 'order', 'advert', 'url', 'vote', 'offhours') NOT NULL DEFAULT 'text' COMMENT '������������� ���������/��� ������ (mention - ����������)'");
	$this->alterColumn('messages', 'message_state', "ENUM('received','updated','deleted') NOT NULL DEFAULT 'received' COMMENT '��������� ��������� (received - �������� ��������, updated - ���������)'");
	$this->alterColumn('messages', 'message_update', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� ���������� ����������'");
	$this->alterColumn('messages', 'message_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");

	/* ������� � �������� � �������� ��������� */
	$this->alterColumn('delivmessagesusers', 'delivmessageuser_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");

	/* ������� � ������� ����������� ������ */
	$this->dropColumn('files', 'chat_id');
	$this->addColumn('files', 'session_id', "INT UNSIGNED NOT NULL COMMENT '���-������ � ������� ���� ���� �����������'");
	$this->alterColumn('files', 'file_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");

	/* ������� � ������� �� ��������� ������������� ����� */
	$this->alterColumn('filesaccesses', 'fileaccess_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");

	/* ������� � ������� ������ */
	$this->alterColumn('emotions', 'emotion_create', "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'");


        $this->createTable('automessages',[
		'automessage_id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT '���������� �������������'",
		'user_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '������ � �������������'",
		'group_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '������ � �������'",
		'rule_id' => "INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '������ � �������� ��������'",
		'automessage_type' => "ENUM('Rules', 'Allocation', 'Greeting', 'Pending') NOT NULL COMMENT '������������� ���� ��������� (Welkome - ��������������, Pending - � �������� ��������, Vote - ��������� ������ ������ ����� �� ��������) '",
		'message_type' => "ENUM('text', 'emotion', 'file', 'mention', 'product', 'order', 'advert', 'url', 'vote', 'greeting', 'offhours', 'allocation') NOT NULL DEFAULT 'text' COMMENT '������������� ���������/��� ������'",
		'message_data' => "TEXT NOT NULL COMMENT '���������� ��������� � json ������� {\'text\':\'asdasd [[f:0]] wew [[e:2]]\', \'html\':\'\', \'files\':[{\'id\':\'\',\'progress\':\'\',\'name\':\'\',\'size\':\'\',\'url\':\'\'}], \'emotions\':[{\'emotion_id\':\'\'}]}'",
		'automessage_create' => "TIMESTAMP(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000' COMMENT '���� �������� ������'",
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->createIndex('automessage_type_unique', 'automessages', ['automessage_type', 'user_id', 'group_id', 'rule_id'], true);

    }

    public function down()
    {
        echo "m160429_102025_refactoring cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
