<?php

use yii\db\Schema;
use yii\db\Migration;

class m160203_054338_create_settings_tbl extends Migration
{
    public function up()
    {
        $this->createTable('settings', [
            'id' => 'int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'code' => "varchar(30) NOT NULL DEFAULT '' ",
            'value' => "varchar(30) NOT NULL DEFAULT '' ",
            'desc' => "TEXT"
        ], 'ENGINE=InnoDB CHARSET=utf8');

         $this->insert('settings', [
            'code' => 'greeting_mess',
            'value' => 'Welcome!',
            'desc' => 'Greeting message'
        ]);    
    }

    public function down()
    {
        echo "m160203_054338_create_settings_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
