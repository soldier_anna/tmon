<?php

use yii\db\Schema;
use yii\db\Migration;

class m160301_044337_alter_orders extends Migration
{
    public function up()
    {
        $this->dropColumn('orders', 'main_deal_srl');
    }

    public function down()
    {
        echo "m160301_044337_alter_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
