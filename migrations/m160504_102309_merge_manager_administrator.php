<?php

use yii\db\Migration;

class m160504_102309_merge_manager_administrator extends Migration
{
    public function up()
    {
	$this->alterColumn('users', 'user_role', "ENUM('customer','agent','administrator') NOT NULL DEFAULT 'customer' COMMENT '���� ������� ������'");
    }

    public function down()
    {
        echo "m160504_102309_merge_manager_administrator cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
