<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;



/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => ['class' => 'app\components\AccessRule'],
                'rules' => [
                    [   
                        'actions' => ['index','clients', 'create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [   
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [   
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    { 
        $searchModel = new UsersSearch();
        $filter = ['role' => 'agent'];
        $params = array_merge($filter, Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search($params);
      
        return $this->render('index',[
           'searchModel' => $searchModel,
           'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClients()
    {
        $searchModel = new UsersSearch();
        $filter = ['role' => 'customer'];
        $params = array_merge($filter, Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search($params);
       
        return $this->render('clients',[
           'searchModel' => $searchModel,
           'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model. To create operator, agent, client
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();
        $model->scenario = Users::SCENARIO_CREATE_ANY_USER;        
        $post = Yii::$app->request->post();  
        $type = Yii::$app->request->get('type'); 
        $type = ($type == 'agent') ? 'agent' : 'customer';     

        if ($post) {
             $post['Users']['user_role'] = $type;           
            // echo $model->scenario;die;
            /*if ($post['Users']['create_at'] !== '') {
                $post['Users']['create_at'] = str_replace('/', '-', $post['Users']['create_at']);
                $post['Users']['create_at'] = date('Y-m-d H:i:s', strtotime($post['Users']['create_at'])); 
            }  */            
        } 
        if ($model->load($post) && $model->save() ) {  
            $action = ($type == 'agent') ? 'index' : 'clients';             
            return $this->redirect(['users/' . $action]);             
        } else {          
            return $this->render('create', [
                'model' => $model,
                
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {  
        $model = $this->findModel($id);      
        $model->scenario = Users::SCENARIO_USER_UPDATE;
        $post = Yii::$app->request->post(); 

        if ($post) {
            if (trim($post['Users']['user_pass']) == '') {
                unset($post['Users']['user_pass']);
            } 
        } 
        /*if ($post && $post['Users']['user_create'] !== '') {         
            $post['Users']['user_create'] = str_replace('/', '-', $post['Users']['user_create']);
            $post['Users']['user_create'] = date('Y-m-d H:i:s', strtotime($post['Users']['user_create']));
                        
        }*/

        if ($model->load($post) && $model->save()) {
            //echo $model->scenario;die;
            $action = ($model->user_role == 'agent') ? 'index' : 'clients';           
            return $this->redirect(['users/' . $action]);
        } else {
            unset($model->user_pass);
            $model->user_create = date('d/m/Y', strtotime($model->user_create));
            return $this->render('update', [
                'model' => $model,                
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = $this->findModel($id);
        $action = ($user->user_role == 'agent') ? 'index' : 'clients';
        $user->delete();

        return $this->redirect([$action]);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    

    

    
}
