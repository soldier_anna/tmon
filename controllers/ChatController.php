<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Groups;

class ChatController extends Controller 
{

	 public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => ['class' => 'app\components\AccessRule'],
                'rules' => [
                    [   
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [   
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [   
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],            
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    { 
        $this->layout = false;
        //$this->layout = 'chat';

        $array = [];   
        $gr = Yii::$app->user->identity->groups;
        foreach ($gr as $k=>$v) {
            $array[$k]['group_id'] = $v->group_id;
            $array[$k]['group_title'] = $v->group_title;
            $array[$k]['group_create'] = $v->group_create;
        }
        
        $groups = json_encode($array);         
        
        return $this->render('index', [
            'currentUserGroups' => $groups
        ]);
    }
}