<?php

namespace app\controllers;

use Yii;
use app\models\Products;
use app\models\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => ['class' => 'app\components\AccessRule'],
                'rules' => [
                    [   
                        'actions' => ['index','view', 'create', 'update','delete', 'image', 'item'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [   
                        'actions' => ['logout','item', 'image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [   
                        'actions' => ['login','image'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

   

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*public function actionImage($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $file = $this->findModel($id);

        if ($file) $file_path = $file->getFilePath(); 
        //var_dump($file_path. $file->filename);die;
        if ($file_path && file_exists($file_path . $file->img)) {
            Yii::$app->response->SendFile($file_path . $file->img);
        }
    }*/
    /*
    Gets items
     */
    public function actionItem($product_id)
    {
        $product_id = (int)$product_id;
        //if (Yii::$app->request->isAjax) {
             $product = Products::find()
                    ->where(['id' => $product_id])->asArray()->all();
            //var_dump($product[0]);

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ($product) ? $product[0] : [];

        //}
    }

   
}
