<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use app\models\OrderProducts;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => ['class' => 'app\components\AccessRule'],
                'rules' => [
                    [   
                        'actions' => ['index','view', 'create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [   
                        'actions' => ['logout', 'item'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [   
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {


        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function saveOrder(\yii\db\ActiveRecord $model )
    {
        $post = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();

        try {          
            if ($post && $model->load($post) && $model->save()) {
                $model->unlinkAll('products', true); 
                if (isset($post['Orders']['products']) && $post['Orders']['products']) {
                    foreach ($post['Orders']['products'] as $productId) {
                        $guModel = new OrderProducts();
                        $guModel->product_id = $productId;
                        $guModel->link('orders', $model);
                    }
                } 
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }     
        } catch (Exception $e) {
            $transaction->rollBack();
        }
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();

        $this->saveOrder($model);

        return $this->render('create', [
            'model' => $model,
        ]);
        
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $this->saveOrder($model);
        
        return $this->render('update', [
            'model' => $model,
        ]);       
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = Orders::find()->where(['id'=> $id])->with('products')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

   

    public function actionItem($order_id = null, $user_id = null)
    {      
       $order_id = (int)$order_id;
       $user_id = (int)$user_id;
       Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

       if ($order_id) {
           $orders = Orders::find()
                    ->with('products')
                    ->where(['id' => $order_id])->asArray()->one();

           return ($orders) ? $orders : [];
       } elseif ($user_id) {
            $orders = Orders::find()
                    ->with('products')
                    ->where(['account_srl' => $user_id])->asArray()->all();

          return ($orders) ? $orders : [];
       } else {
            return [];
       }
    }
}
