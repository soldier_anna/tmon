<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */

//print_r ($model->getProductList()); return;
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'main_buy_srl')->textInput() ?>

     <?php ///echo $form->field($model, 'main_deal_srl')->dropDownList($model->getProductList(), ['prompt' => ''])->label('Product'); ?>

    <?php 
    $orderProducts = [];
    foreach ($model->products as $p) {
        $orderProducts[ $p->id ] = ['Selected'=>'selected'];
    }
    // print '<pre>';
    // print_r ($orderProducts);
    ?>
     <?= $form->field($model, 'products[]')->dropDownList($model->getProductList(),
                        [ 'multiple' => 'multiple',
                        'options' => $orderProducts ]) ?> 

    <?php /* echo $form->field($model, 'main_deal_srl')->textInput() */ ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'state')->dropDownList([ 'cancelled' => 'Cancelled', 'success' => 'Success', 'returned' => 'Returned', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'original_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discount_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coupon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delivery_addr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account_srl')->textInput() ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
