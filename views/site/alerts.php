<?php if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } ?>   
<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success fade in">
         <a href="#" class="close" data-dismiss="alert">&times;</a>
         <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } ?>

<?php
Yii::$app->view->registerJs(  
   '$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");', \yii\web\View::POS_READY
);
?>