<!DOCTYPE html>
<html lang="en">
  <head>
  	<base href="/chat" />
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="TMON">
	<meta name="author" content="">
	<title>TMON</title>
	<link rel="icon" href="favicon.ico" />
	<script type="text/javascript">
		window.currentUserGroups = <?php echo $currentUserGroups ?> ;
		window.currentUserId = <?php echo Yii::$app->user->identity->user_id ?> ;	
		window.userToken = "<?php echo Yii::$app->user->identity->auth_key ?>";
		window.currentUserIsAdmin = <?php echo ((Yii::$app->user->identity->user_role == 'administrator') ? 'true' : 'false') ?>;
	</script>
	<link rel="stylesheet" type="text/css" href="/css/styles.css">	
  </head>
  <body>
    <div id="app"></div>
    <script src="/js/bundle.js"></script>
   </body>
</html>