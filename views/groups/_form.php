<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\Helper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Groups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groups-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'group_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_thumb')->textInput(['maxlength' => true]) ?>
    <?= Html::img($model->group_thumb, ['width' => '100px']) ?>
    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'group_backup')->widget(Select2::className(),[
        'data' => $data['groups'],
        'options' => ['placeholder' => 'Select backup group ...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'group_capacity')->textInput(['maxlength' => true, 'placeholder' => 'Maximum capacity group']) ?>

    <?= $form->field($model, 'groupsusers')->widget(Select2::className(),[
        'data' => $data['users'],
        'options' => [
            'placeholder' => 'Select users for group ...',
            'multiple' => true
        ],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>











