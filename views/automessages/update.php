<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Automessages */

$this->title = 'Update Automessages: ' . $model->automessage_id;
$this->params['breadcrumbs'][] = ['label' => 'Automessages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->automessage_id, 'url' => ['view', 'id' => $model->automessage_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="automessages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
