<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Automessages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="automessages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rule_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'automessage_type')->dropDownList([ 'Rules' => 'Rules', 'Allocation' => 'Allocation', 'Greeting' => 'Greeting', 'Pending' => 'Pending', 'OffHours' => 'OffHours', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'message_type')->dropDownList([ 'text' => 'Text', 'emotion' => 'Emotion', 'file' => 'File', 'mention' => 'Mention', 'product' => 'Product', 'order' => 'Order', 'advert' => 'Advert', 'url' => 'Url', 'auto_vote' => 'Auto vote', 'auto_greeting' => 'Auto greeting', 'auto_allocation' => 'Auto allocation', 'auto_offhours' => 'Auto offhours', 'btn_order' => 'Btn order', 'btn_lastseen' => 'Btn lastseen', 'btn_basket' => 'Btn basket', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'message_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'automessage_create')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
