<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Automessages */

$this->title = $model->automessage_id;
$this->params['breadcrumbs'][] = ['label' => 'Automessages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="automessages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->automessage_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->automessage_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'automessage_id',
            'user_id',
            'group_id',
            'rule_id',
            'automessage_type',
            'message_type',
            'message_data:ntext',
            'automessage_create',
        ],
    ]) ?>

</div>
