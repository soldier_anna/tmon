<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AutomessagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="automessages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'automessage_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'group_id') ?>

    <?= $form->field($model, 'rule_id') ?>

    <?= $form->field($model, 'automessage_type') ?>

    <?php // echo $form->field($model, 'message_type') ?>

    <?php // echo $form->field($model, 'message_data') ?>

    <?php // echo $form->field($model, 'automessage_create') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
