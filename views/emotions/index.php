<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmotionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emotions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emotions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Emotions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'emotion_id',
            'emotion_key',
            'emotion_title',
            'emotion_url:ntext',
            'emotion_state',
            // 'emotion_create',

            ['class' => 'yii\grid\ActionColumn'],

            [
                'attribute' => 'emotion_url',
                'format' => 'raw',
                'filter' => false,
                'value' => function($data) {
                    return  \yii\helpers\Html::img($data->getImageFileUrl('emotion_url'),[
                        'alt' => 'Emotions',
                        'style' => 'width:60px;'
                    ]);
                }
            ],
        ],
    ]); ?>

</div>
