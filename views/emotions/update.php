<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Emotions */

$this->title = 'Update Emotions: ' . ' ' . $model->emotion_id;
$this->params['breadcrumbs'][] = ['label' => 'Emotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->emotion_id, 'url' => ['view', 'id' => $model->emotion_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emotions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
