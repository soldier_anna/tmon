<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Emotions */

$this->title = 'Emotion "' . $model->emotion_title . '"';
$this->params['breadcrumbs'][] = ['label' => 'Emotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emotions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->emotion_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->emotion_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'emotion_id',
            'emotion_key',
            'emotion_title',           
            'emotion_state',
            'emotion_create',
            [
                'label'  => 'Img',
                'value'  => '<img width="100px" src="' . $model->getImageFileUrl('emotion_url') . ' " >', 
                'format' => 'html'
            ],
        ],
    ]) ?>

</div>
