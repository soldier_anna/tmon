<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chats".
 *
 * @property integer $chat_id
 * @property integer $user_id
 * @property integer $product_id
 * @property integer $order_id
 * @property integer $group_id
 * @property string $chat_type
 * @property string $chat_state
 * @property string $chat_create
 */
class Chats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id', 'order_id', 'group_id'], 'integer'],
            [['chat_type', 'chat_state'], 'string'],
            [['chat_create'], 'safe'],
            [['user_id', 'product_id', 'order_id', 'group_id'], 'unique', 'targetAttribute' => ['user_id', 'product_id', 'order_id', 'group_id'], 'message' => 'The combination of User ID, Product ID, Order ID and Group ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'chat_id' => 'Chat ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'order_id' => 'Order ID',
            'group_id' => 'Group ID',
            'chat_type' => 'Chat Type',
            'chat_state' => 'Chat State',
            'chat_create' => 'Chat Create',
        ];
    }
}
