<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $role
 * @property string $fname
 * @property string $lname
 * @property string $phone
 * @property string $address
 * @property string $state
 * @property string $create_at
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_USER_UPDATE = 'user_update';
    const SCENARIO_CREATE_ANY_USER = 'create_any_user';
    const SCENARIO_API_REG = 'api_reg';
    const SCENARIO_API_UPDATE = 'api_update';
   /* 
    const SCENARIO_CREATE_CLIENT = 'create_client';
    
    */

    public $confirm_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        
        $scenarios[self::SCENARIO_USER_UPDATE] = [ 'user_pass', 'confirm_password', 'user_nick', 'user_state', 'user_capacity', 'user_create'];
        $scenarios[self::SCENARIO_CREATE_ANY_USER] = ['user_login', 'user_pass', 'confirm_password','user_nick', 'user_role', 'user_state', 'user_capacity', 'user_create'];
      
        return $scenarios;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_login', 'user_pass'], 'required', 'except' => self::SCENARIO_USER_UPDATE],
            [['user_role', 'user_state'], 'string'],
            [['user_capacity'], 'integer'],
            [['user_create'], 'safe'],
            [['user_login', 'user_pass', 'password_reset_token', 'user_nick'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            
            [['user_login'], 'unique', 'message' => '{attribute} {value} is already used in system'],         
            [['auth_key'], 'required', 'on' => 'update_auth_key'],
            ['confirm_password', 'compare', 'compareAttribute' => 'user_pass'],
            [['confirm_password'], 'required', 'on' => [self::SCENARIO_CREATE_ANY_USER]],
            ['user_login', 'email', 'on' => self::SCENARIO_CREATE_ANY_USER, 'message' => 'Login should be email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {        
        return [
            'user_id' => 'ID',
            'user_login' => 'Login',
            'user_pass' => 'Password',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'user_nick' => 'Nickname',
            'user_role' => 'Role',         
            'user_state' => 'State',
            'user_create' => 'Date created',
            'confirm_password' => 'Confirm password'
        ];
    }

    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['group_id' => 'group_id'])
            ->viaTable('groupsusers', ['user_id' => 'user_id']);
    }

    public function getRules()
    {
         return $this->hasMany(Rules::className(), ['rule_id' => 'rule_id']) //->where('rulesgroupsusers.user_id <> "0"')
            ->viaTable('rulesgroupsusers', ['user_id' => 'user_id']);
    }


    /** INCLUDE USER LOGIN VALIDATION FUNCTIONS**/
        /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
/* modified */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }
 
    /**
     * Finds user by user_login
     *
     * @param  string      $user_login
     * @return static|null
     */
    public static function findByUsername($user_login)
    {
        return static::find()
            ->where(['user_login' => $user_login])
            //->andWhere(['user_role' => 'administrator'])
            ->andWhere(['or', ['user_role' => 'administrator'], ['user_role' => 'agent']])
            ->one();
        //return static::findOne(['user_login' => $user_login])
            
        ;
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
//Yii::info("validateAuthKey", 'apiResponse');
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($user_pass)
    {
        return $this->user_pass === sha1(mb_strtoupper($this->user_login) . ":" . $user_pass);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($user_pass)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($user_pass);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /** EXTENSION MOVIE **/

//    protected function loginByCookie()
//    {
//        parent::loginByCookie();
//	if ($this->getIsGuest()) {
//            $value = Yii::$app->getRequest()->getPosts()->getValue($this->identityCookie['name']);
//        }
//	return;
//        $value = Yii::$app->getRequest()->getCookies()->getValue($this->identityCookie['name']);
//        if ($value === null) {
//            return;
//        }
//
//        $data = json_decode($value, true);
//        if (count($data) !== 3 || !isset($data[0], $data[1], $data[2])) {
//            return;
//        }
//
//        list ($id, $authKey, $duration) = $data;
//        /* @var $class IdentityInterface */
//        $class = $this->identityClass;
//        $identity = $class::findIdentity($id);
//        if ($identity === null) {
//            return;
//        } elseif (!$identity instanceof IdentityInterface) {
//            throw new InvalidValueException("$class::findIdentity() must return an object implementing IdentityInterface.");
//        }
//
//        if ($identity->validateAuthKey($authKey)) {
//            if ($this->beforeLogin($identity, true, $duration)) {
//                $this->switchIdentity($identity, $this->autoRenewCookie ? $duration : 0);
//                $ip = Yii::$app->getRequest()->getUserIP();
//                Yii::info("User '$id' logged in from $ip via cookie.", __METHOD__);
//                $this->afterLogin($identity, true, $duration);
//            }
//        } else {
//            Yii::warning("Invalid auth key attempted for user '$id': $authKey", __METHOD__);
//        }
//    }

    

    public function getStatus($state)
    {
        $statuses = array(
            'Training' => 'Training',
            'Lunch' => 'Lunch',
            'O/B' => 'O/B',
            'Online' => 'Online',
        );

        return $statuses[$state];
    }

    
    

    public function beforeSave($insert) {

        if (parent::beforeSave($insert)) {

            if ($insert || $this->getOldAttribute("user_pass") != $this->user_pass) {
                $this->user_pass = sha1(mb_strtoupper($this->user_login) . ":" . $this->user_pass); 
            }
            //if ($this->isNewRecord) {
                //$this->auth_key = \Yii::$app->security->generateRandomString();
            //}

            return true;
        } else {
            return false;
        }
      
        //return parent::beforeSave($insert);
    }

   

}
