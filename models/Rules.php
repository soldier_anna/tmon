<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rules".
 *
 * @property string $id
 * @property string $id_user
 * @property string $id_event
 * @property string $create_at
 */
class Rules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id', 'rule_priority'], 'integer'],
            [['rule_info', 'rule_title'], 'required'],
            [['rule_info', 'rule_state'], 'string'],
            [['rule_create'], 'safe'],
            [['rule_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rule_id' => 'Rule ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'rule_title' => 'Rule Title',
            'rule_info' => 'Rule Info',
            'rule_priority' => 'Rule Priority',
            'rule_state' => 'Rule State',
            'rule_create' => 'Rule Create',
        ];
    }
}
