<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "groupsusers".
 *
 * @property integer $groupuser_id
 * @property integer $user_id
 * @property integer $group_id
 * @property string $groupuser_create
 */
class Groupsusers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groupsusers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id'], 'required'],
            [['user_id', 'group_id'], 'integer'],
            [['groupuser_create'], 'safe'],
            [['user_id', 'group_id'], 'unique', 'targetAttribute' => ['user_id', 'group_id'], 'message' => 'The combination of User ID and Group ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'groupuser_id' => 'Groupuser ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'groupuser_create' => 'Groupuser Create',
        ];
    }

    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['group_id' => 'group_id']);
    }

    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['user_id' => 'user_id']);
    }
}
