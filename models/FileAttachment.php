<?php

namespace app\models;

use Yii;
use yii\base\Model;

/** * Class Person *
 * @package common\models *
 * @package common\models *
 * @property int $id unique person identifier *
 * @property string $name person / user name *
 * @property array $avatar generated filename on server *
 * @property string $filename source filename from client */

class FileAttachment extends Model
{
	/** @var mixed image the attribute for rendering the file input
	 * widget for upload on the form     */
	public $files;

	public function rules()
	{
		return
			[
				[['files'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 2, 'extensions'=> ['png', 'jpeg', 'gif', 'jpg'], 'checkExtensionByMimeType' => false],
			];
	}

	public function upload($uploadDirectory)
	{
		foreach ($this->files as $file)
		{
			$file_name = hash('crc32', $file->baseName) . '.' . $file->extension;
			if ($file->saveAs($uploadDirectory . $file_name)) {
				return $file_name;
			};
		}
		return false;
	}
}