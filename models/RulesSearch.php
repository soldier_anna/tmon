<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rules;

/**
 * RulesSearch represents the model behind the search form about `app\models\Rules`.
 */
class RulesSearch extends Rules
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rule_id', 'user_id', 'group_id'], 'integer'],
            [['rule_title', 'rule_info', 'rule_create', 'rule_state', 'rule_priority'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rules::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'rule_id' => $this->rule_id,
            'rule_create' => $this->rule_create,
        ]);

        $query->andFilterWhere(['like', 'rule_title', $this->rule_title])
            ->andFilterWhere(['like', 'rule_info', $this->rule_info]);

        return $dataProvider;
    }
}
