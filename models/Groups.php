<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\models\Groupsusers;

/**
 * This is the model class for table "groups".
 *
 * @property integer $group_id
 * @property string $group_title
 * @property string $group_create
 */
class Groups extends \yii\db\ActiveRecord
{
    // ----------------------
    public $groupsusers;

    public function updateUsers() {
        if (!is_array($this->groupsusers)) return false;

        Groupsusers::deleteAll(['group_id' => $this->group_id]);
        foreach ($this->groupsusers as $keyC => $valC) {
            $model = new Groupsusers();
            $model->group_id = $this->group_id;
            $model->user_id = $valC;
            $model->save();
        }

        return true;
    }

    // ----------------------
    public $imageFile;

    public function upload() {
        if (!isset($this->group_id)) return false;
        if (!isset($this->imageFile)) return true;

        $UploadDir = Yii::getAlias(Yii::$app->params['uploadGroups'] . $this->group_id . "/");
        if (is_dir($UploadDir) || mkdir($UploadDir, 0755, true)) {
            // Чистим каталог
            $files = glob($UploadDir . '*', GLOB_MARK);
            foreach ($files as $file) unlink($file);

            $file_name = hash('crc32', $this->imageFile->baseName) . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs($UploadDir . $file_name)) {
                $this->group_thumb = Yii::$app->params['pathGroups'] . $this->group_id . "/" . $file_name;
                $this->update(true, ['group_thumb']);
                return true;
            }
        }
        return false;
    }

    public function save($runValidation = true, $attributeNames = NULL) {
        $status = parent::save($runValidation, $attributeNames);
        if ($status) {
            $this->updateUsers();
            $this->upload();
        }
        return $status;
    }

    public function load($data, $formName = NULL) {
        $status = parent::load($data, $formName);
        if ($status) $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        return $status;
    }
    //---------------------------
    public $agents;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    public function behaviors()
    {
        return [
            \cornernote\linkall\LinkAllBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_thumb'], 'string'],
            [['group_backup', 'group_capacity'], 'integer'],
            [['group_backup', 'group_capacity'], 'default', 'value' => 0],

            [['group_create'], 'safe'],
            [['group_title'], 'string', 'max' => 255],

            [['groupsusers'], 'each', 'rule' => ['integer']],

            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg', 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'group_thumb' => 'Group Thumb',
            'group_title' => 'Group Title',
            'group_backup' => 'Group Backup ID',
            'group_capacity' => 'Group Capacity',
            'group_create' => 'Group Create',
            'groupsusers' => 'Agents in group',
        ];
    }

    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['user_id' => 'user_id'])
            ->viaTable('groupsusers', ['group_id' => 'group_id']);
    }

    public function arrayToStr($array)
    {
	$sList = "";
	foreach ($array as $key => $value) {
	    $sList .= $value["user_login"] . ", ";
	}
        return $sList;
    }

    public function getGroupsusers()
    {
        return $this->hasMany(Users::className(), ['user_id' => 'user_id'])->viaTable(Groupsusers::tableName(), ['group_id' => 'group_id']);
    }

    public function getBackup()
    {
        return $this->hasOne(Groups::className(), ['group_id' => 'group_backup']);
    }

    // public function getAgentsList()
    // {
    //     $users = \app\models\Users::find()
    //                     ->select(['user_id', 'user_nick'])
    //                     ->where(['user_role' => 'agent', 'user_state' => 'active'])
    //                     ->all();
    //     $agents = ArrayHelper::map($users, 'user_id', 'user_nick');

    //     Yii::info($agents, 'requests');

    //     return $agents;

    // }
}
