<?php

namespace app\models;

use Yii;
use app\models\Orders;
use yii\helpers\Url;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $main_deal_srl
 * @property string $name
 * @property string $description
 * @property string $delivery
 * @property string $price
 * @property string $category_srl
 * @property string $img
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_deal_srl', 'name', 'description', 'category_srl'], 'required'],
            [['main_deal_srl'], 'integer'],
            [['description', 'delivery'], 'string'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 100],
            [['category_srl'], 'string', 'max' => 200],
            //[['img'], 'string', 'max' => 255],
            ['img', 'file', 'extensions' => 'jpeg, gif, png, jpg'], 
        ];
    }

    public $config =  [
             'class' => '\yiidreamteam\upload\ImageUploadBehavior',
             'attribute' => 'img',
             'thumbs' => [
                 'thumb' => ['width' => 300, 'height' => 200],
             ],
             'filePath' => '@webroot/uploads/[[basename]]', //'@app/runtime/uploads/[[pk]]/[[basename]]',
             'fileUrl' => '/uploads/[[basename]]',
             //'fileUrl' =>  '/index.php?r=products/image&id=[[pk]]',// /index.php?r=products/image&id=[[pk]] // Url::toRoute(['products/image', 'id' =>  $this->id ]) , // '/products/image/[[pk]].[[extension]]',
             //'thumbPath' => '@app/runtime/uploads/thumbs/[[pk]]/[[basename]]',
            ];

    public function behaviors()
    {
        return [
           $this->config,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_deal_srl' => 'Main Deal Srl',
            'name' => 'Name',
            'description' => 'Description',
            'delivery' => 'Delivery',
            'price' => 'Price',
            'category_srl' => 'Category Srl',
            'img' => 'Img',
        ];
    }

    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['id' => 'order_id'])
            ->viaTable('order_products', ['product_id' => 'id']);
    }

    // public function getOrders()
    // {
    //     return $this->hasMany(Orders::className(), ['main_deal_srl' => 'main_deal_srl']);
    // }

    public function getFilePath() {

        if (!isset($this->id)) return null;
        return  $this->config['filePath'];

        // $dir_f = Yii::getAlias($this->config['filePath'] . $this->id . "/");
        // if (is_dir($dir_f) || mkdir($dir_f, 0755, true)) {
        //     return $dir_f;
        // }
        // return null;
    }
}
