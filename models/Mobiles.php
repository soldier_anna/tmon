<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mobiles".
 *
 * @property string $id
 * @property string $id_user
 * @property string $device_type
 * @property string $device_token
 * @property string $create_at
 */
class Mobiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mobiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'device_token'], 'required'],
            [['id_user'], 'integer'],
            [['device_type', 'device_token'], 'string'],
            [['create_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'device_type' => 'Device Type',
            'device_token' => 'Device Token',
            'create_at' => 'Create At',
        ];
    }

    
}
