<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pushes".
 *
 * @property integer $push_id
 * @property string $push_type
 * @property string $push_key_origin
 * @property string $push_key_failed
 * @property string $push_data
 * @property string $push_state
 * @property string $push_info_send
 * @property string $create_at
 */
class Pushes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pushes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['push_type'], 'required'],
            [['push_type', 'push_key_origin', 'push_key_failed', 'push_data', 'push_state', 'push_info_send'], 'string'],
            [['create_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'push_id' => 'Push ID',
            'push_type' => 'Push Type',
            'push_key_origin' => 'Push Key Origin',
            'push_key_failed' => 'Push Key Failed',
            'push_data' => 'Push Data',
            'push_state' => 'Push State',
            'push_info_send' => 'Push Info Send',
            'create_at' => 'Create At',
        ];
    }
}
