<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emotions".
 *
 * @property integer $emotion_id
 * @property string $emotion_key
 * @property string $emotion_title
 * @property string $emotion_url
 * @property string $emotion_state
 * @property string $emotion_create
 */
class Emotions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emotions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emotion_key', 'emotion_state'], 'required'],
            //[['emotion_url', 'emotion_state'], 'string'],
            ['emotion_url', 'file', 'extensions' => 'jpeg, gif, png, jpg'], 
            [['emotion_create'], 'safe'],
            [['emotion_key', 'emotion_title'], 'string', 'max' => 255],
            [['emotion_key'], 'unique']
        ];
    }

    public $config =  [
             'class' => '\yiidreamteam\upload\ImageUploadBehavior',
             'attribute' => 'emotion_url',
             'thumbs' => [
                 'thumb' => ['width' => 300, 'height' => 200],
             ],
             'filePath' => '@webroot/emotions/[[basename]]', //'@app/runtime/emotions/[[pk]]/[[basename]]',
             'fileUrl' => '/emotions/[[basename]]',
             //'fileUrl' =>  '/index.php?r=products/image&id=[[pk]]',// /index.php?r=products/image&id=[[pk]] // Url::toRoute(['products/image', 'id' =>  $this->id ]) , // '/products/image/[[pk]].[[extension]]',
             //'thumbPath' => '@app/runtime/emotions/thumbs/[[pk]]/[[basename]]',
            ];

    public function behaviors()
    {
        return [
           $this->config,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'emotion_id' => 'Emotion ID',
            'emotion_key' => 'Emotion Key',
            'emotion_title' => 'Emotion Title',
            'emotion_url' => 'Emotion Url',
            'emotion_state' => 'Emotion State',
            'emotion_create' => 'Emotion Create',
        ];
    }

    public function getFilePath() {

        if (!isset($this->id)) return null;
        return  $this->config['filePath'];
        
    }
}
