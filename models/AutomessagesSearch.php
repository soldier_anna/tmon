<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Automessages;

/**
 * AutomessagesSearch represents the model behind the search form about `app\models\Automessages`.
 */
class AutomessagesSearch extends Automessages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['automessage_id', 'user_id', 'group_id', 'rule_id'], 'integer'],
            [['automessage_type', 'message_type', 'message_data', 'automessage_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Automessages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'automessage_id' => $this->automessage_id,
            'user_id' => $this->user_id,
            'group_id' => $this->group_id,
            'rule_id' => $this->rule_id,
            'automessage_create' => $this->automessage_create,
        ]);

        $query->andFilterWhere(['like', 'automessage_type', $this->automessage_type])
            ->andFilterWhere(['like', 'message_type', $this->message_type])
            ->andFilterWhere(['like', 'message_data', $this->message_data]);

        return $dataProvider;
    }
}
