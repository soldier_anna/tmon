<?php

namespace app\models;

use Yii;
use app\models\Products;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $main_buy_srl
 * @property integer $main_deal_srl
 * @property string $date
 * @property integer $count
 * @property string $state
 * @property string $original_price
 * @property string $discount_price
 * @property string $coupon
 * @property string $delivery_addr
 * @property integer $account_srl
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    public function behaviors()
    {
        return [
            \cornernote\linkall\LinkAllBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_buy_srl',  'account_srl'], 'required'],
            [['main_buy_srl',  'count', 'account_srl'], 'integer'],
            [['date'], 'safe'],
            [['state'], 'string'],
            [['original_price', 'discount_price'], 'number'],
            [['coupon'], 'string', 'max' => 100],
            [['delivery_addr'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_buy_srl' => 'Main Buy Srl',
           // 'main_deal_srl' => 'Main Deal Srl',
            'date' => 'Date',
            'count' => 'Count',
            'state' => 'State',
            'original_price' => 'Original Price',
            'discount_price' => 'Discount Price',
            'coupon' => 'Coupon',
            'delivery_addr' => 'Delivery Addr',
            'account_srl' => 'Account Srl',
        ];
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])
            ->viaTable('order_products', ['order_id' => 'id']);
    }

    // public function getProduct()
    // {
    //     return $this->hasOne(Products::className(), ['main_deal_srl' => 'main_deal_srl']);
    // }

    public function getProductList()
    {
        // $products = $this->getProducts()->select(['id', 'name'])->asArray()->all();
        // return ArrayHelper::map($products, 'id', 'name');
        $products = Products::find()->select(['id', 'name'])->asArray()->all();
        return ArrayHelper::map($products, 'id', 'name');
    }
}
