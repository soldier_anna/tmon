<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "automessages".
 *
 * @property string $automessage_id
 * @property string $user_id
 * @property string $group_id
 * @property string $rule_id
 * @property string $automessage_type
 * @property string $message_type
 * @property string $message_data
 * @property string $automessage_create
 */
class Automessages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'automessages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id', 'rule_id'], 'integer'],
            [['automessage_type', 'message_data'], 'required'],
            [['automessage_type', 'message_type', 'message_data'], 'string'],
            [['automessage_create'], 'safe'],
            [['automessage_type', 'user_id', 'group_id', 'rule_id'], 'unique', 'targetAttribute' => ['automessage_type', 'user_id', 'group_id', 'rule_id'], 'message' => 'The combination of User ID, Group ID, Rule ID and Automessage Type has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'automessage_id' => 'Automessage ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'rule_id' => 'Rule ID',
            'automessage_type' => 'Automessage Type',
            'message_type' => 'Message Type',
            'message_data' => 'Message Data',
            'automessage_create' => 'Automessage Create',
        ];
    }
}
