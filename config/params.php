<?php

return [
    'adminEmail' => 'admin@example.com',
    'pushKeyAndroid' => 'AIzaSyAwPamYOkac-mG3LLpvFpLO0esaSXZop8A',
    'pushKeyIos' => '',
    'uploadFile' => '@app/runtime/uploads/',
    'uploadGroups' => '@webroot/uploads/groups/',
    'pathGroups' => 'uploads/groups/',
];
