var 	config = require('./config'),
	//rcluster = require('../lib/redis-cluster/index'),
	rcluster = require('../lib/redis-cluster2/index'),
	redis = require("redis");

module.exports = Redis;

function Redis() {
	this.connectDatabase();
}

function Redis(onconnect) {
	this.onconnect = onconnect;
	this.connectDatabase();
}

Redis.prototype.connectDatabase = function() {
	var pending = 2;
	var self = this;

	self.rcluster = rcluster;

	// Redis входящие для подписывания
	self.rc_inc = rcluster.createClient(config.REDIS_CLUSTER_INFO)
//	self.rc_inc = redis.createClient(config.REDIS_INFO);
	self.rc_inc.on('connect', function () {
		console.log('Redis "incoming" connected.')
		if (0 === --pending && typeof self.onconnect === 'function') self.onconnect();
	});
	self.rc_inc.on("error", function (err) {
		console.log('Redis "incoming" error: ' + err);
	});


	// Redis исходщий для отправки сообщений
	self.rc_out = rcluster.createClient(config.REDIS_CLUSTER_INFO)
//	self.rc_out = redis.createClient(config.REDIS_INFO);
	self.rc_out.on('connect', function () {
		console.log('Redis "outgoing" connected.')
		if (0 === --pending && typeof self.onconnect === 'function') self.onconnect();
	});
	self.rc_out.on("error", function (err) {
		console.log('Redis "outgoing" error: ' + err);
	});

}

