function makerandstr(lenstr)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < lenstr; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

function checkerrors(data) {
	if (typeof data.errors === 'undefined') return false;	

	console.log('Error: ', data);
	alert('Errors see logs.');
	return true;
}

function updateUsers(relations_chats, chat_id) {
	$('div#list_users').html("");

	relations_chats[chat_id].forEach(function(value, key, origin) {
		$('div#list_users').html($('div#list_users').html() + value.user_nick + "(" + value.user_state + ", " + value.chatuser_state + "), ");
	});
}

function appendMessage(message) {
	$('div[name=chat_window]').append(message);
	$("div[name=chat_window]").animate({
		scrollTop: $("div[name=chat_window]")[0].scrollHeight
	}, 100);
}

$(function() {
	var users_info = {};
        var relations_chats = []; // чаты и отношение пользоватлей к ним {chat_id: {user_id:{chatuser_state:""}}} 
        var active_chats = [];
	var socket;

		// $("#logout").click(function(){
		// 	console.log('logout...');
		// 	socket.disconnect();
		// });

	// Выход
	$(document).on('click','input[name=logout]',function(){
		console.log('< logout');
		socket.disconnect();
	});

	// Список чатов
	$(document).on('click','input[name=all_chats]',function(){
		console.log('< get all chats');
		socket.emit('all_chats', {});
	});
	$(document).on('click','input[name=me_chats]',function(){
		console.log('< get me chats');
		socket.emit('me_chats', {});
	});
	$(document).on('click','input[name=empty_chats]',function(){
		console.log('< get empty chats');
		socket.emit('empty_chats', {});
	});

	// Подаем запрос на вход в чат
	$(document).on('click','a.chat_choiсe',function(){
		console.log('< choice chat', $(this).attr('id'));
		socket.emit('relation_chat', {chat_id:$(this).attr('id').split('_')[1], chatuser_state:'request'});
	});

	// Подаем запрос на вход в чат
	$(document).on('change','input[name=typing]',function(){
		if ($('div#cur_chat').html() <=0) return;
		console.log('< typing: ', this.checked);
		if (this.checked) socket.emit('relation_chat', {chat_id:$('div#cur_chat').html(), chatuser_state:'typing'});
		else socket.emit('relation_chat', {chat_id:$('div#cur_chat').html(), chatuser_state:'active'});
//		socket.emit('relation_chat', {chat_id:$(this).attr('id').split('_')[1], chatuser_state:'request'});
	});

	// Отправляем рекламное сообщение
	$(document).on('submit','#universal',function(){
		if ($('div#cur_chat').html() > 0) {
			var rand_str = generateUUID();

			var data = {chat_id:$('div#cur_chat').html(),message_type:$('select[name=universal_type]').val(),message_data:{title:$('input[name=universal_title]').val(),text:$('input[name=universal_text]').val(),price:$('input[name=universal_price]').val(),image:$('input[name=universal_image]').val(),click:$('input[name=universal_click]').val(),order_id:$('input[name=universal_order_id]').val(),order_state:$('input[name=universal_order_state]').val(),order_date:$('input[name=universal_order_date]').val()},message_key:rand_str};
			console.log('< universal message', data);

			if ($('select[name=universal_from]').val() == "system") socket.emit('tech_message', data);
			else socket.emit('send_message', data);
		}
		return false;
	});


	// Отправляем сообщение 
	$(document).on('submit','#universal2',function(){
		var rand_str = generateUUID();

		var data = {
			message_type:$('select[name=universal2_type]').val(),
			message_data:{title:$('input[name=universal2_title]').val(),
				text:$('input[name=universal2_text]').val(),
				price:$('input[name=universal2_price]').val(),
				image:$('input[name=universal2_image]').val(),
				click:$('input[name=universal2_click]').val(),
				order_id:$('input[name=universal2_order_id]').val(),
				order_state:$('input[name=universal2_order_state]').val(),
				order_date:$('input[name=universal2_order_date]').val()},
			message_key:rand_str
		};
		if ($('input[name=universal2_chat_id]').val() > 0) data["chat_id"] = $('input[name=universal2_chat_id]').val();
		if ($('input[name=universal2_user_id]').val() > 0) data["user_id"] = $('input[name=universal2_user_id]').val();
		if ($('input[name=universal2_group_id]').val() > 0) data["group_id"] = $('input[name=universal2_group_id]').val();

		console.log('< universal message 3', data);

		if ($('select[name=universal2_from]').val() == "user") socket.emit('user_message_v3', data);
		else if ($('select[name=universal2_from]').val() == "system") socket.emit('system_message_v3', data);

		return false;
	});

	$(document).on('submit','#universal3',function(){
		var rand_str = generateUUID();

		var data = {
			chatrelation_role:$('select[name=universal3_role]').val(),
			chatrelation_state:$('select[name=universal3_state]').val()
		};
		if ($('input[name=universal3_chat_id]').val() > 0) data["chat_id"] = $('input[name=universal3_chat_id]').val();
		if ($('input[name=universal3_user_id]').val() > 0) data["user_id"] = $('input[name=universal3_user_id]').val();
		if ($('input[name=universal3_group_id]').val() > 0) data["group_id"] = $('input[name=universal3_group_id]').val();

		console.log('< universal relation 3', data);

		if ($('select[name=universal3_from]').val() == "user") socket.emit('user_relation_v3', data);
		else if ($('select[name=universal3_from]').val() == "system") socket.emit('system_relation_v3', data);

		return false;
	});


	$(document).on('submit','#universal4',function(){
		var data = {user_id:$('input[name=universal4_user_id]').val()};

		console.log('< user info 3', data);
		socket.emit('user_info_v3', data);

		return false;
	});


	$(document).on('submit','#universal5',function(){
		var data = {chat_id:$('input[name=universal5_chat_id]').val()};

		console.log('< chat users 3', data);
		socket.emit('chat_relations_v3', data);

		return false;
	});

	$(document).on('submit','#universal6',function(){
		var type = $('select[name=universal6_type]').val();
		var data = {};

		if ($('input[name=universal6_user_id]').val() > 0) data["user_id"] = $('input[name=universal6_user_id]').val();
		if ($('input[name=universal6_group_id]').val() > 0) data["group_id"] = $('input[name=universal6_group_id]').val();

		console.log('<- ' + type, data);
		socket.emit(type, data);

		return false;
	});


	$(document).on('submit','#universal7',function(){
		var data = {user_id:$('input[name=universal7_user_id]').val()};

		console.log('<- User info 7', data);
		socket.emit('user_info_v3', data);

		return false;
	});


	$(document).on('submit','#universal8',function(){
		var data = {};

		if ($('input[name=universal8_chat_id]').val() > 0) data["chat_id"] = $('input[name=universal8_chat_id]').val();
		if ($('input[name=universal8_user_id]').val() > 0) data["user_id"] = $('input[name=universal8_user_id]').val();

		console.log('<- Chat history 8', data);
		socket.emit('chat_history_v3', data);

		return false;
	});


	$(document).on('submit','#universal9',function(){
		var data = {};

		if ($('input[name=universal9_message_id]').val() > 0) data["message_id"] = $('input[name=universal9_message_id]').val();
		if ($('input[name=universal9_user_id]').val() > 0) data["user_id"] = $('input[name=universal9_user_id]').val();

		console.log('<- Delivery message 9', data);
		socket.emit('message_delivery_v3', data);

		return false;
	});

	// Запрашиваем данные участников чата
	$(document).on('click', 'input[name=chat_users]', function(){
		console.log('< chat_users', $('div#cur_chat').html());

		socket.emit('chat_users', {chat_id:$('div#cur_chat').html()});

		return false;
	});

	// Запрос хистори чата
	$(document).on('click', 'input[name=chat_history]', function(){
		console.log('< get_history', $('div#cur_chat').html());

		var data = {};
		if ($('input[name=ch_chat_id]').val() > 0) data["chat_id"] = $('input[name=ch_chat_id]').val();
		if ($('input[name=ch_user_id]').val() > 0) data["user_id"] = $('input[name=ch_user_id]').val();
		if ($('input[name=ch_product_id]').val() > 0) data["product_id"] = $('input[name=ch_product_id]').val();
		if ($('input[name=ch_order_id]').val() > 0) data["order_id"] = $('input[name=ch_order_id]').val();
		if ($('input[name=ch_group_id]').val() > 0) data["group_id"] = $('input[name=ch_group_id]').val();

		socket.emit('get_history', data);

		return false;
	});

	// Открывает/Создаем чат продукции
	$(document).on('click', 'input[name=open_chat]', function(){
		console.log('< open_chat', $('div#cur_chat').html());

		socket.emit('open_chat', {product_id:$('input[name=product_id]').val()});

		return false;
	});


	$(document).on('submit','#login',function(){
		var user_id = $('input[name="user_id"]').val();
		
		socket = io('/chat', { query: "user_id=" + user_id + "&user_token=-1" });

		socket.on('disconnect', function(data) {
			socket.close();
			console.log('> disconnect');
			$('div#online_form').hide();	// Скрываем страницу чата
			$('div#login_form').show();	// Выводим форму авторизации
			$('input[name=send]').attr('disabled','disabled');	// Блокируем кнопку отправки
			$('div#list_chats').empty();	// Чистим список доступных чатов
			$('div#chat_window').hide();	// Скрываем окно чата
			$('div#cur_chat').empty();	// Чистим ид текущего чата
			$('div#list_users').empty();	// Чистим список участников чата
		});

		socket.on('login', function(data) {
			console.log('> login');
			$('div#login_form').hide();	// Скрываем форму авторизации
			$('div#online_form').show();	// Выводим страницу чата
		});

		// Список всех чатов
		socket.on('all_chats', function(data) {
			console.log('> list_chats', data);
			if (checkerrors(data)) return;

			$('div#list_chats').empty();
			data.list.forEach(function(value, key, origin) {
				$('div#list_chats').append('<a href=#chat_type=' + value["chat_type"] + '&chat_id=' + value["chat_id"] + ' class="chat_choiсe" id=chat_' + value["chat_id"] + '>(id' + value["chat_id"] + "_u" + value["user_id"] + "_p" + value["product_id"] + "_o" + value["order_id"] + "_g" + value["group_id"] + ')</a>, &nbsp;&nbsp;&nbsp;');
			});
		});
		socket.on('me_chats', function(data) {
			console.log('> list_chats', data);
			if (checkerrors(data)) return;

			$('div#list_chats').empty();
			data.list.forEach(function(value, key, origin) {
				$('div#list_chats').append('<a href=#chat_type=' + value["chat_type"] + '&chat_id=' + value["chat_id"] + ' class="chat_choiсe" id=chat_' + value["chat_id"] + '>(id' + value["chat_id"] + "_u" + value["user_id"] + "_p" + value["product_id"] + "_o" + value["order_id"] + "_g" + value["group_id"] + ')</a>, &nbsp;&nbsp;&nbsp;');
			});
		});
		socket.on('empty_chats', function(data) {
			console.log('> list_chats', data);
			if (checkerrors(data)) return;

			$('div#list_chats').empty();
			data.list.forEach(function(value, key, origin) {
				$('div#list_chats').append('<a href=#chat_type=' + value["chat_type"] + '&chat_id=' + value["chat_id"] + ' class="chat_choiсe" id=chat_' + value["chat_id"] + '>(id' + value["chat_id"] + "_u" + value["user_id"] + "_p" + value["product_id"] + "_o" + value["order_id"] + "_g" + value["group_id"] + ')</a>, &nbsp;&nbsp;&nbsp;');
			});
		});


		// Уведомление о изменении отновшения к чату 
		socket.on('relation_chat', function(data) {
			console.log('> relation_chat', data);
			if (checkerrors(data)) return;

			$('div#chat_window').show();
			$('div#cur_chat').html(data.chat_id);

			if (typeof relations_chats[data.chat_id] === 'undefined') relations_chats[data.chat_id] = [];
			relations_chats[data.chat_id][data.user_id] = {user_nick:data.user_nick,user_state:data.user_state,chatuser_role:data.chatuser_role,chatuser_state:data.chatuser_state};

			$('input[name=send]').removeAttr('disabled');

			var value = "<font color='#777777'>User id: " + data.user_id + ", chat id: " + data.chat_id + ", chatuser state: " + data.chatuser_state + "</font><br/>\n";
			appendMessage(value);

			updateUsers(relations_chats, data.chat_id);
		});
		socket.on('relation_v3', function(data) {
			console.log('> relation v3', data);
			if (checkerrors(data)) return;

			$('div#chat_window').show();
			$('div#cur_chat').html(data.chat_id);

			if (typeof relations_chats[data.chat_id] === 'undefined') relations_chats[data.chat_id] = [];
			relations_chats[data.chat_id][data.user_id] = {user_nick:data.user_nick,user_state:data.user_state,chatuser_role:data.chatuser_role,chatuser_state:data.chatuser_state};

			$('input[name=send]').removeAttr('disabled');

			var value = "<font color='#777777'>User id: " + data.user_id + ", chat id: " + data.chat_id + ", chatuser state: " + data.chatuser_state + "</font><br/>\n";
			appendMessage(value);

			updateUsers(relations_chats, data.chat_id);
		});

		// Получение нового сообщения
		socket.on('send_message', function(data) {
			console.log('> send message', data);
			if (checkerrors(data)) return;

			var value = "Chat id: " + data.chat_id + ", message key: " + data.message_key + ", user id: " + data.user_id + ", message type: " + data.message_type + ", message data: " + JSON.stringify(data.message_data) + "<br/>\n";
			appendMessage(value);
		});
		socket.on('message_v3', function(data) {
			console.log('> message v3', data);
			if (checkerrors(data)) return;

			var value = "Chat id: " + data.chat_id + ", message key: " + data.message_key + ", user id: " + data.user_id + ", message type: " + data.message_type + ", message data: " + JSON.stringify(data.message_data) + "<br/>\n";
			appendMessage(value);
		});



		// Полная информация по конкретному пользователю
		socket.on('user_info_v3', function(data) {
			console.log('> user info v3', data);

//			users_info[data.user_id] == data;

			var value = "<font color='#777777'>User info : " + JSON.stringify(data) + "</font><br/>\n";
			appendMessage(value);

			// Надо обновить список участников чата
		});


		// Информация об участниках чата
		socket.on('chat_relations_v3', function(data) {
			console.log('> chat relations v3', data);
			if (checkerrors(data)) return;

			var value = "<font color='#777777'>Update info all users chat.</font><br/>\n";
			appendMessage(value);

			if (typeof relations_chats[data.chat_id] === 'undefined') relations_chats[data.chat_id] = [];
			data.relations.forEach(function(value, key, origin) {
				relations_chats[data.chat_id][value.user_id] = {user_nick:value.user_nick, user_state:value.user_state, chatuser_state:value.chatuser_state};
			});
			updateUsers(relations_chats, data.chat_id);
		});
		socket.on('chat_users', function(data) {
			console.log('> chat_users', data);
			if (checkerrors(data)) return;

			var value = "<font color='#777777'>Update info all users chat.</font><br/>\n";
			appendMessage(value);

			if (typeof relations_chats[data.chat_id] === 'undefined') relations_chats[data.chat_id] = [];
			data.users.forEach(function(value, key, origin) {
				relations_chats[data.chat_id][value.user_id] = {user_nick:value.user_nick, user_state:value.user_state, chatuser_state:value.chatuser_state};
			});
			updateUsers(relations_chats, data.chat_id);
		});



		// Сменился статус пользователя в одном из чатов
		socket.on('status_v3', function(data) {
			console.log('> user status', data);

			users_info[data.user_id] == data;

			var value = "<font color='#777777'>User id: " + data.user_id + ", chat id: " + data.chat_id + ", user status: " + data.user_status + "</font><br/>\n";
			appendMessage(value);

			// Надо обновить список участников чата
		});

		socket.on('user_state', function(data) {
			console.log('> user_state', data);
			if (checkerrors(data)) return;

			if (typeof relations_chats[data.chat_id] === 'undefined') relations_chats[data.chat_id] = [];
			relations_chats[data.chat_id][data.user_id] = {user_nick:data.user_nick,user_state:data.user_state,chatuser_role:data.chatuser_role,chatuser_state:data.chatuser_state};

			var value = "<font color='#777777'>User id: " + data.user_id + ", chat id: " + data.chat_id + ", user state: " + data.user_state + "</font><br/>\n";
			appendMessage(value);

			updateUsers(relations_chats, data.chat_id);
		});

		// Хистори чата пришло
		socket.on('list_history', function(data) {
			console.log('> list_history', data);
			if (checkerrors(data)) return;

// Надо хранить текущее хистори сообщений, чтобы сверить с пришедшими чтобы распределить их в чате правильно
			var value = "<font color='#777777'>Chat id: " + data.chat_id + ", message_create: " + data.message_create + ", count equal: " + data.equal.length + ", count later: " + data.later.length + " equals: " + JSON.stringify(data.equal) + " laters: " + JSON.stringify(data.later) + "</font><br/>\n";

			appendMessage(value);

		});

		// Информирование о назначении менеджера
		socket.on('assign_manager_v3', function(data) {
			console.log('> assign manager', data);

			var value = "<font color='#777777'>Chat id: " + data.chat_id + " - assign manager: " + JSON.stringify(data.manager_info) + "</font><br/>\n";
			appendMessage(value);
		});


		// Список чатов для указанной группы
		socket.on('group_chats_v3', function(data) {
			console.log('> group chats', data);

			var value = "<font color='#777777'>Group id: " + data.group_id + " - list chats: " + JSON.stringify(data.list_chats) + "</font><br/>\n";
			appendMessage(value);
		});

		// Список чатов для без активных агентов или групп
		socket.on('empty_chats_v3', function(data) {
			console.log('> empty chats', data);

			var value = "<font color='#777777'>Empty list chats: " + JSON.stringify(data.list_chats) + "</font><br/>\n";
			appendMessage(value);
		});

		// Список чатов для без активных агентов или групп
		socket.on('user_chats_v3', function(data) {
			console.log('> user chats', data);

			var value = "<font color='#777777'>User id: " + data.user_id + " - list chats: " + JSON.stringify(data.list_chats) + "</font><br/>\n";
			appendMessage(value);
		});


		// Список чатов для без активных агентов или групп
		socket.on('all_chats_v3', function(data) {
			console.log('> all chats', data);

			var value = "<font color='#777777'>List all chats: " + JSON.stringify(data.list_chats) + "</font><br/>\n";
			appendMessage(value);
		});


		// История сообщений указанного чата
		socket.on('chat_history_v3', function(data) {
			console.log('> chat history', data);

			var value = "<font color='#777777'>History - Chat id: " + data.chat_id + ", message_create: " + data.message_create + ", count equal: " + data.equal.length + ", count later: " + data.later.length + " equals: " + JSON.stringify(data.equal) + " laters: " + JSON.stringify(data.later) + "</font><br/>\n";
			appendMessage(value);
		});


		socket.on('message_delivery_v3', function(data) {
			console.log('> message delivery', data);

			var value = "<font color='#777777'>Delivery - Chat id: " + data.chat_id + ", message_id: " + data.message_id + ", user_id: " + data.user_id + "</font><br/>\n";
			appendMessage(value);
		});



		return false;
	});




});